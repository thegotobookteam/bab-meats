<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <artifactId>bab-meats-web</artifactId>
    <groupId>com.stgconsulting.bab.bab-meats</groupId>
    <version>${release}.${buildNumber}</version>
    <name>web</name>
    <packaging>jar</packaging>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.1.RELEASE</version>
        <relativePath/>
        <!-- lookup parent from repository -->
    </parent>

    <properties>
        <release>1</release>
        <buildNumber>LOCAL</buildNumber>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>11</java.version>
        <bab.core.version>1.LOCAL</bab.core.version>
        <janino.version>2.7.8</janino.version>
        <jersey-core.version>1.18</jersey-core.version>
        <h2-database-version>1.4.199</h2-database-version>
        <guava.version>27.1-jre</guava.version>
        <!--        <spring.boot.version>2.1.4.RELEASE</spring.boot.version>-->
        <dockerfile-maven-version>1.4.10</dockerfile-maven-version>
        <spring-cloud.version>Hoxton.SR5</spring-cloud.version>
    </properties>

    <scm>
        <developerConnection>scm:git:https://dqromney@bitbucket.org/thegotobookteam/bab-meats.git</developerConnection>
    </scm>

    <dependencies>
        <!--Spring Boot dependencies-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
        </dependency>
        <!-- Spring Data Rest -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-rest</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>

        <!-- Spring Cloud -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>

        <dependency>
            <groupId>org.codehaus.janino</groupId>
            <artifactId>janino</artifactId>
            <version>${janino.version}</version>
        </dependency>
        <dependency>
            <groupId>com.sun.jersey</groupId>
            <artifactId>jersey-core</artifactId>
            <version>${jersey-core.version}</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-entitymanager</artifactId>
            <version>5.4.2.Final</version>
            <scope>runtime</scope>
        </dependency>

        <!-- util -->
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>${guava.version}</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.6</version>
        </dependency>

        <!-- BAB dependencies -->
        <dependency>
            <groupId>com.stgconsulting.bab</groupId>
            <artifactId>bab-core-model</artifactId>
            <version>${bab.core.version}</version>
        </dependency>
        <dependency>
            <groupId>com.stgconsulting.bab</groupId>
            <artifactId>bab-core-web</artifactId>
            <version>${bab.core.version}</version>
        </dependency>
        <dependency>
            <groupId>com.stgconsulting.bab</groupId>
            <artifactId>bab-meats-model</artifactId>
            <version>${release}.${buildNumber}</version>
        </dependency>

        <!-- Database dependencies -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>runtime</scope>
            <version>${h2-database-version}</version>
        </dependency>

        <!-- SWAGGER2 dependencies -->
        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-annotations</artifactId>
            <version>1.5.9</version>
        </dependency>

        <!-- TEST dependencies -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <executable>true</executable>
                </configuration>
            </plugin>
            <!-- Spring Boot Logback Configuration -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.1.1</version>
                <executions>
                    <execution>
                        <id>unpack</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <includeGroupIds>com.stgconsulting.bab</includeGroupIds>
                            <includeArtifactIds>logging</includeArtifactIds>
                            <excludeTransitive>true</excludeTransitive>
                            <outputDirectory>${project.build.directory}/classes</outputDirectory>
                            <includes>logback-spring.xml</includes>
                            <overWriteReleases>true</overWriteReleases>
                            <overWriteSnapshots>true</overWriteSnapshots>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.7</version>
                <executions>
                    <execution>
                        <id>make-version-file</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target name="buildinfo">
                                <tstamp>
                                    <format property="buildDate" pattern="MM/dd/yyyy hh:mm aa z"/>
                                </tstamp>
                                <exec executable="whoami" outputproperty="whoami"/>
                                <propertyfile file="src/main/resources/build.properties"
                                              comment="This file is automatically generated - DO NOT EDIT">
                                    <entry key="DATE" value="${buildDate}"/>
                                    <entry key="REVISION" value="${planRepositoryRevision}"/>
                                    <entry key="VERSION" value="${project.version}"/>
                                    <entry key="BUILD" value="${buildNumber}"/>
                                    <entry key="JOB" value="${planKey}"/>
                                </propertyfile>
                            </target>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- Dockerfile Maven: https://github.com/spotify/dockerfile-maven/blob/master/docs/usage.md -->
            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>dockerfile-maven-plugin</artifactId>
                <version>${dockerfile-maven-version}</version>
                <executions>
                    <execution>
                        <id>default</id>
                        <goals>
                            <goal>build</goal>
                            <goal>push</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <skip>true</skip>
                    <contextDirectory>${project.basedir}</contextDirectory> <!-- Required -->
                    <repository>${project.artifactId}</repository>
                    <tag>${project.version}</tag>
                    <buildArgs>
                        <JAR_FILE>${project.build.finalName}.jar</JAR_FILE>
                        <!--suppress UnresolvedMavenProperty -->
                        <!--                        <SSH_PRIVATE_KEY>${SSH_PRIVATE_KEY}</SSH_PRIVATE_KEY>-->
                        <!--suppress UnresolvedMavenProperty -->
                        <!--                        <SSH_PUBLIC_KEY>${SSH_PUBLIC_KEY}</SSH_PUBLIC_KEY>-->
                    </buildArgs>
                    <skipDockerInfo>true</skipDockerInfo>
                    <!-- Not sure if this is needed - dqr -->
                    <useMavenSettingsForAuth>true</useMavenSettingsForAuth>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <repositories>
        <repository>
            <id>spring-milestones</id>
            <name>Spring Milestones</name>
            <url>https://repo.spring.io/milestone</url>
        </repository>
    </repositories>

</project>
