package com.stgconsulting.bab.meats.web.dao;

import com.stgconsulting.bab.meats.model.Meat;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bun Row Mapper class.
 */
public class MeatsRowMapper implements RowMapper<Meat> {

    @Override
    public Meat mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Meat meat = new Meat();

        meat.setId(resultSet.getLong("id"));
        meat.setName(resultSet.getString("name"));
        meat.setDescription(resultSet.getString("description"));
        meat.setImagePath(resultSet.getString("image_path"));

        return meat;
    }
}
