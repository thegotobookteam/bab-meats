package com.stgconsulting.bab.meats.web.controller;

import com.google.common.base.Preconditions;
import com.stgconsulting.bab.meats.model.Meat;
import com.stgconsulting.bab.meats.web.service.MeatService;
import com.stgconsulting.bab.meats.web.util.RestPreconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.stgconsulting.bab.meats.model.constants.Headers.MEAT_ID;
import static com.stgconsulting.bab.meats.model.constants.Resources.*;

@Slf4j
@RestController
@RequestMapping(REST_PREFIX)
public class Meats {

    private MeatService meatService;

    /**
     * DI Constructor
     */
    public Meats(MeatService pMeatService) {
        meatService = pMeatService;
    }

    @GetMapping(value = API_MEAT_ALL_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Meat> findAll() {
        //log.info("findAll() :: Enter/Exit");
        return meatService.findAll();
    }

    @GetMapping(value = API_MEAT_BY_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    public Meat findById(@Valid @PathVariable(MEAT_ID) Long pMeatId) {
        //log.info("findById() :: Enter/Exit");
        return RestPreconditions.checkFound(meatService.findOne(pMeatId));
    }

    @PostMapping(value = API_MEAT_CREATE_URI, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Integer create(@RequestBody Meat pMeat) {
        //log.info("create() :: Enter/Exit");
        Preconditions.checkNotNull(pMeat);
        return meatService.create(pMeat);
    }

    @PutMapping(value = API_MEAT_UPDATE_URI, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody Meat pMeat) {
        //log.info("update() :: Enter");
        Preconditions.checkNotNull(pMeat);
        RestPreconditions.checkFound(meatService.findOne(pMeat.getId()));
        meatService.update(pMeat);
        //log.info("update() :: Exit");
    }

    @DeleteMapping(value = API_MEAT_DELETE_BY_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable(MEAT_ID) Long pId) {
        //log.info("deleteById() :: Enter");
        meatService.deleteById(pId);
        //log.info("deleteById() :: Exit");
    }

    @GetMapping(value = API_MEAT_IMAGE_BY_ID_URI, produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    byte[] getImage(@Valid @PathVariable(MEAT_ID) Long pMeatId) throws IOException {
        //log.info("getImage() :: Enter");
        Meat meat = meatService.findOne(pMeatId);
        InputStream in = getClass().getResourceAsStream("/images/" + meat.getImagePath());
        //log.info("getImage() :: Exit");
        return IOUtils.toByteArray(in);
    }
}
