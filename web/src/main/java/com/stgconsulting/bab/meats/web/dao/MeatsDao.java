package com.stgconsulting.bab.meats.web.dao;

import com.stgconsulting.bab.meats.model.Meat;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Meats DAO or Data Access Object.
 */
@Repository
@NoArgsConstructor
public class MeatsDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(final DataSource pDataSource) {
        this.jdbcTemplate = new JdbcTemplate(pDataSource);
        final CustomSQLErrorCodeTranslator customSQLErrorCodeTranslator = new CustomSQLErrorCodeTranslator();
        jdbcTemplate.setExceptionTranslator(customSQLErrorCodeTranslator);
    }

    public int getCount() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM BUNS", Integer.class);
    }

    public List<Meat> findAll() {
        return jdbcTemplate.query("SELECT * FROM MEATS", new MeatsRowMapper());
    }

    public Meat findOne(final long pMeatId) {
        final String query = "SELECT * FROM MEATS WHERE ID = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{pMeatId}, new MeatsRowMapper());
    }

    public Integer create(final Meat pMeat) {
        return jdbcTemplate.update("INSERT INTO MEATS VALUES (?, ?, ?, ?)",
                pMeat.getId(), pMeat.getName(), pMeat.getDescription(), pMeat.getImagePath());
    }

    public Integer update(final Meat pMeat) {
        return jdbcTemplate.update("UPDATE MEATS SET name = ?, description = ?, file_image = ? WHERE ID = ?",
                pMeat.getName(), pMeat.getDescription(), pMeat.getImagePath(), pMeat.getId());
    }

    public Integer delete(final Long pId) {
        return jdbcTemplate.update("DELETE FROM MEATS WHERE ID = ?", pId);
    }
}
