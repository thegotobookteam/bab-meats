package com.stgconsulting.bab.meats.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.PropertySource;

@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
@PropertySource("classpath:build.properties")
public class MeatsApp {

    public static void main(String[] args) {
        //log.info("main() :: Enter");

        SpringApplication.run(MeatsApp.class, args);

        //log.info("main() :: Exit");
    }
}
