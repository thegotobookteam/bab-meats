package com.stgconsulting.bab.meats.web.service;

import com.stgconsulting.bab.meats.model.Meat;
import com.stgconsulting.bab.meats.web.dao.MeatsDao;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Bun service.
 */
@Service
public class MeatService {

    private MeatsDao meatsDao;

    public MeatService(MeatsDao meatsDao) {
        this.meatsDao = meatsDao;
    }

    public Meat findOne(Long pId) {
        return meatsDao.findOne(pId);
    }

    public List<Meat> findAll() {
        return meatsDao.findAll();
    }

    public Integer create(Meat pMeat) {
        return meatsDao.create(pMeat);
    }

    public Integer update(Meat pMeat) {
        return meatsDao.update(pMeat);
    }

    public Integer deleteById(Long pId) {
        return meatsDao.delete(pId);
    }

}
