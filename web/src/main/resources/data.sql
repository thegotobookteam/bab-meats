-- Resource site: https://www.saveur.com/gallery/Classic-Buns-and-Breads/
DROP TABLE IF EXISTS meats;

CREATE TABLE meats
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    name        VARCHAR(250)  NOT NULL,
    description VARCHAR(4096) NOT NULL,
    image_path  VARCHAR(250) DEFAULT NULL
);

INSERT INTO meats (name, description, image_path)
VALUES ('Brisket',
        'Sometimes we crave lean burgers; other times, we want the handheld equivalent of a dry-aged steak. The cuts and styles of beef featured here are available in most markets and can be used on their own or blended together. Buy ground meat, or ask your butcher to grind it for you; many supermarket meat counters are equipped for the task.',
        'https://www.saveur.com/resizer/9plJj8-iMgdCN6go1NKN12xy9SE=/400x287/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/5E2BJVQGVGOOLUOPBHC3H3RJX4.jpg'),
       ('Chuck',
        'The well-marbled and full-flavored shoulder, or chuck, of the steer has a near- perfect ratio of meat to fat when ground (80 to 20 is considered ideal for burgers).',
        'https://www.saveur.com/resizer/tAW8oXwu82oAMLmmNyhINOs5MAE=/380x212/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/EBG2PSDT54FRDPWMK7MFNH7EEM.jpg'),
       ('Dry-Aged Beef',
        'Including some ground dry-aged beef (from cuts like the rib eye, pictured) will give your burgers a concentrated, steak-like flavor.',
        'https://www.saveur.com/resizer/AUOJa3SgZnIPHoh2Yq2hGeDosXg=/386x198/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/KRI57YVUMH455P3AR6Q3TF45IA.jpg'),
       ('Grass-Fed',
        'Less fatty than corn-fed beef, the meat of pasture-raised cattle produces a lean hamburger with a clean, mineral flavor. You can add a bit of ground fat to your blend for more flavor.',
        'https://www.saveur.com/resizer/2Wef0tclH85RC9fcajv1Jo_4tG0=/396x225/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/V4LEML22TTQN2PVNW3KNJRPJ4Y.jpg'),
       ('Wagyu',
        'In the U.S., Japanese Wagyu cows have been crossbred with those of other breeds to produce unsurpassably tender, melt-in-your-mouth beef. Ground Wagyu makes for a seriously luxe burger.',
        'https://www.saveur.com/resizer/hrg0NRq_CfBGOV64Ls3LzSiRs4k=/388x247/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/3BX2BOCREXFGNCAJRFJPLU4NUM.jpg'),
       ('Short Rib',
        'Short ribs are meaty and tender and make for a particularly sumptuous burger. Ask your butcher to grind boneless short rib, also called chuck flap tail.',
        'https://www.saveur.com/resizer/9hKKJ-SpbVroppvG4MaklywJ8FM=/365x223/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/HXBR3JTDSICUWVI7UWLVRE47TM.jpg'),
       ('Sirloin',
        'Sirloin cuts are amply marbled and full of beefy flavor. When you want the flavor of the patty to stand out, go with top sirloin, tri-tip, or knuckle.',
        'https://www.saveur.com/resizer/C01HLwXancdSQJHyWEuINWwICo4=/400x244/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/D3Y7MMNOJUO7E7BZLQWP7AFSPM.jpg'),
       ('Ground Hamburger',
        'What''s labeled "hamburger" in the supermarket is typically a blend of trimmings from various steaks and roasts. Often, the fat percentage is indicated on the label.',
        'https://www.saveur.com/resizer/FYuL970N3tuokH14qERI733WHcg=/365x267/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/GSUMDKEHEII2DOLOU2M3BLQSQY.jpg'),
       ('Round',
        'The hindquarters, or round, has little intramuscular fat. Once ground, it makes for a lean patty that takes well to rich accompaniments.',
        'https://www.saveur.com/resizer/jiThRyrjR3ytQeWBLVxOA8J9T1M=/400x400/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/4VATYY2CPKETJQWYI4RQEZWLOU.jpg');
