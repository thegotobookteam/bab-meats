package com.stgconsulting.bab.meats.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Meat model.
 */
@Data
@NoArgsConstructor
@Entity
public class Meat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "id", required = true, dataType = "Long", example = "123")
    private Long id;

    @ApiModelProperty(value = "name", required = true, dataType = "String", example = "Sliced Ham")
    private String name;

    @ApiModelProperty(value = "description", required = true, dataType = "String", example = "Thin Sliced Black Forest Ham.")
    private String description;

    @ApiModelProperty(value = "image_path", required = true, dataType = "String", example = "https://www.saveur.com/resizer/9plJj8-iMgdCN6go1NKN12xy9SE=/400x287/arc-anglerfish-arc2-prod-bonnier.s3.amazonaws.com/public/5E2BJVQGVGOOLUOPBHC3H3RJX4.jpg")
    @JsonProperty(value = "image_path")
    private String imagePath;
}
