package com.stgconsulting.bab.meats.model.constants;

import static com.stgconsulting.bab.meats.model.constants.Headers.MEAT_ID;

/**
 * Resource constants.
 *
 * Note:
 * By convention, such fields have names consisting of capital letters, with words separated by underscores.
 * It is critical that these fields contain either primitive values or references to immutable objects.
 */
public class Resources {

    public static final String REST_PREFIX = "/rest";
    public static final String BASE_RESOURCE = "/meats";

    public static final String API_MEAT_ALL_URI = BASE_RESOURCE;
    public static final String API_MEAT_ALL_DESC = "Retrieve list of all hamburger meats.";

    public static final String API_MEAT_BY_ID_URI = BASE_RESOURCE + "/{" + MEAT_ID + "}";
    public static final String API_MEAT_BY_ID_DESC = "Retrieve details of a hamburger meat.";

    public static final String API_MEAT_CREATE_URI = BASE_RESOURCE;
    public static final String API_MEAT_CREATE_DESC = "Create a type of hamburger meat.";

    public static final String API_MEAT_UPDATE_URI = BASE_RESOURCE;
    public static final String API_MEAT_UPDATE_DESC = "Update a hamburger meat information.";

    public static final String API_MEAT_DELETE_BY_ID_URI = BASE_RESOURCE + "/{" + MEAT_ID + "}";
    public static final String API_MEAT_DELETE_BY_ID_DESC = "Delete a hamburger meat.";

    public static final String API_MEAT_IMAGE_BY_ID_URI = BASE_RESOURCE + "/{" + MEAT_ID + "}/image";
    public static final String API_MEAT_IMAGE_BY_ID_DESC = "Retrieve meat image.";

}
