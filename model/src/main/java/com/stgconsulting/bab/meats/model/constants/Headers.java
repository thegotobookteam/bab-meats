package com.stgconsulting.bab.meats.model.constants;

/**
 * Header constants.
 *
 * Note:
 * By convention, such fields have names consisting of capital letters, with words separated by underscores.
 * It is critical that these fields contain either primitive values or references to immutable objects.
 */
public class Headers {

    // Header parameters
    public static final String MEAT_ID = "meat_id";
}
